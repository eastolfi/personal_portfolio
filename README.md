# Estado del proyecto #

![Codeship Build](https://codeship.com/projects/de03a160-25a1-0134-f1a5-16799befcf0e/status?branch=master)

# Instrucciones de instalación del proyecto

[Descargar el repositorio](https://bitbucket.org/eastolfi/personal_portfolio/get/material_design.zip) y descomprimirlo en cualquier carpeta.

Descargar e instalar NodeJS

Abrir una consola de comandos en el directorio del proyecto.

Lanzamos el comando npm install, y cuando termine lazamos el comando node server.js  .Despues accedemos en el navegador a http://localhost.3000