## Variables ##

# Global Packages #

grunt = ./node_modules/.bin/grunt

# Building #
build_app = $(grunt) build_app

# Testing #
run_test = $(grunt) test

## Actions ##

# Testing #

test:
	#$(run_test)
	npm test
	
# Building #

build:
	$(build_app)

## Publishg ##

# NPM #

npm_major: test
	npm version major --no-git-tag-version
	git add . --all
	git commit -m "VERSION: New major version released"

npm_minor: test
	npm version minor --no-git-tag-version
	git add . --all
	git commit -m "VERSION: New minor version released"

npm_patch: test
	npm version patch --no-git-tag-version
	git add . --all
	git commit -m "VERSION: New patch released"

# Bower #

bower_major: test
	bower version major -m "VERSION: New major version released (v%s)"
	git push -u origin --follow-tags

bower_minor: test
	bower version minor -m "VERSION: New minor version released (v%s)"
	git push -u origin --follow-tags

bower_patch: test
	bower version patch -m "VERSION: New patch released (v%s)"
	git push -u origin --follow-tags
	
# NPM & Bower #
	
publish_major:
	make npm_major
	make bower_major
	npm publish
	
publish_minor:
	make npm_minor
	make bower_minor
	npm publish
	
publish_patch:
	make npm_patch
	make bower_patch
	npm publish
	
.PHONY: build_all, publish_major, publish_minor, publish_patch