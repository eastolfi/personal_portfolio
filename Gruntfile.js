module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'public/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/css',
                    ext: '.min.css'
                }]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'public/css/styles.css': 'public/sass/global.scss'
                }
            },
            mat: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'public/css/styles_material.css': 'public/sass/global_material.scss'
                }
            },
            application: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'public/css/general_app_styles.css': 'public/sass/general_app_styles.scss'
                }
            }
        },
        clean: {
            bower: ['bower_components'],
            npm: ['node_modules/*', '!node_modules/grunt*/**']
        },
        shell: {
            install: {
                command: 'npm install'
            }
        }
    });
    
    // Load the plugins
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    // grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-shell');
    
    grunt.registerTask('delete_deps', ['clean:bower', 'clean:npm']);
    grunt.registerTask('fresh_install', ['delete_deps', 'shell:install']);
    
    grunt.registerTask('default', ['fresh_install', 'sass:dist']);
};