"use strict";

/*!
 * Start Bootstrap - Creative Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

$(document).ready(function() {
	// jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });
	
	// Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });
	
	// Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });
	
	// Fit Text Plugin for Main Header
    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    });
	
	$('#toggle-github').click(function(){
		if($(this).hasClass('opened')){
			$(this).removeClass('opened');
			$('#github-profile').animate({'right':'-500px'});
			$('#my-github').css('height', '');
		}else{
			$(this).addClass('opened');
			$('#github-profile').animate({'right':'-10px'});
			if ($(window).height() - 150 < 480) {
				$('#my-github').height(($(window).height() - 150) * 0.7);
			}
		}
	});
	
	
	/*============================================
	Skills Functions
	==============================================*/
	var aboutColor = $('#about').css('backgroundColor');

	$('#services').waypoint(function(){
		$('.chart').each(function(){
		$(this).easyPieChart({
				size:170,
				animate: 2000,
				lineCap:'butt',
				scaleColor: false,
				barColor: aboutColor,
				lineWidth: 10
			});
		});
	},{offset:'80%'});
	
	
	/*============================================
	Filter Projects
	==============================================*/
	$('#filter-works a').click(function(e){
		e.preventDefault();

		$('#filter-works li').removeClass('active');
		$(this).parent('li').addClass('active');

		var category = $(this).attr('data-filter');

		$('.project-item').each(function(){
			if($(this).is(category)){
				$(this).removeClass('filtered');
			}
			else{
				$(this).addClass('filtered');
			}

			$('#projects-container').masonry('reload');
		});

		scrollSpyRefresh();
		waypointsRefresh();
	});
	
	/*============================================
	Backstretch Images
	==============================================*/
	$.backstretch('public/img/header-bg.jpg');

	$('body').append('<img class="preload-image" src="public/img/contact-bg.jpg" style="display:none;"/>');

	$('#about').waypoint(function(direction){
	
		if($('.preload-image').length){$('.preload-image').remove();}
		
		$('.backstretch').remove();
	
		if (direction=='down'){
			$.backstretch('public/img/contact-bg.jpg');
		}else{
			$.backstretch('public/img/header-bg.jpg');
		}
	});
	
	/*============================================
	Refresh scrollSpy function
	==============================================*/
	function scrollSpyRefresh(){
		setTimeout(function(){
			$('body').scrollspy('refresh');
		},1000);
	}

	/*============================================
	Refresh waypoints function
	==============================================*/
	function waypointsRefresh(){
		setTimeout(function(){
			$.waypoints('refresh');
		},1000);
	}
	/*****************************************/

    // Initialize WOW.js Scrolling Animations
    new WOW().init();
	
	
	/*============================================
	Project thumbs - Masonry
	==============================================*/	
	$('#projects-container').css({visibility:'visible'});

	$('#projects-container').masonry({
		itemSelector: '.project-item:not(.filtered)',
		columnWidth:350,
		isFitWidth: true,
		isResizable: true,
		isAnimated: !Modernizr.csstransitions,
		gutterWidth: 0
	});scrollSpyRefresh();
	waypointsRefresh();
	
	/* GitHub */	
	GitHubActivity.feed({
		username: "eastolfi",
		selector: "#my-github",
		limit: 5
	});
		
});
