angular.module('portfolio.index').controller('indexController', 
	['$scope', 'GithubActivityService', '$translate', '$mdSidenav', '$mdUtil',
	function($scope, GithubActivityService, $translate, $mdSidenav, $mdUtil) {
	
	/*********** GREETINGS ************/
	$scope.isGreetingsPopOpen = false;
	$scope.greetingsPopover = {
	    templateUrl: 'template/greetings.html',
	    title: 'GREETINGS_TITLE'
  	};
  	
	$scope.toggleGreetingPopover = function() {
		$scope.isGreetingsPopOpen = !$scope.isGreetingsPopOpen;
	};
	
	
	/*********** SKILLS ************/
    $scope.skills = [{
		'label': 'HTML / CSS',
		'link': 'works',
		'filter': 'pages',
		'percent': '100',//70
		'icon': 'fa-html5', //'icon': 'html5, code',
		'desc': 'SKILL_HTML_CSS'
	},
	{
		'label': 'JavaScript',
		'link': 'works',
		'filter': 'pages',
		'percent': '100',//75
		'icon': 'icon-nodejs',
		'desc': 'SKILL_JS'
	},
	{
		'label': 'Android',
		'link': 'works',
		'filter': 'apps',
		'percent': '100',//65
		'icon': 'fa-android',
		'desc': 'SKILL_ANDROID'
	},
	{
		'label': 'Java',
		'link': 'hiring',
		'percent': '100',//80
		'icon': 'icon-java-bold',
		'desc': 'SKILL_JAVA'
	}];
	
	$scope.doFilter = function(filter) {
		if (filter) {
			$scope.workFilter = filter;
		}
	};
	
	/*********** WORKS ************/
	$scope.workFilter = '*';
	$scope.workFilters = [{
		'label': 'WORKS_ALL',
		'filter': '*'
	},
	{
		'label': 'WORKS_PAGES',
		'filter': 'pages'
	},
	{
		'label': 'WORKS_APPS',
		'filter': 'apps'
	}];
	
	$scope.categoryFilter = function(item) {
		var res = false;
		
		if ($scope.workFilter === '*' || $scope.workFilter === item.filter) {
			res = true;
		}
		
        return  res;
    }
	
	var allWorks = [{
		'category': 'WORKS_PAGES',
		'name': 'WORK_TIME_MANAGMENT',
		'filter': 'pages',
		'link': 'https://gestion-horas-eastolfi.c9.io',
		'img_ref': 'img_time',
		'image': 'public/img/portfolio/1.jpg',
		'order': 1
	},
	{
		'category': 'WORKS_PAGES',
		'name': 'WORK_QUINIELAS',
		'filter': 'pages',
		'link': '#',
		'img_ref': 'img_quinielas',
		'image': 'public/img/portfolio/2.jpg',
		'order': 2
	},
	{
		'category': 'WORKS_PAGES',
		'name': 'WORK_CTTS',
		'filter': 'pages',
		'link': '#',
		'img_ref': 'img_ctts',
		'image': 'public/img/portfolio/3.jpg',
		'order': 3
	},
	{
		'category': 'WORKS_PAGES',
		'name': 'WORK_IMAGE_VIEWER',
		'filter': 'pages',
		'link': '#',
		'img_ref': 'img_viewer',
		'image': 'public/img/portfolio/4.jpg',
		'order': 4
	},
	{
		'category': 'WORKS_PAGES',
		'name': 'WORK_PAGE_GENERATOR',
		'filter': 'pages',
		'link': '#',
		'img_ref': 'img_generator',
		'image': 'public/img/portfolio/5.jpg',
		'order': 5
	},
	{
		'category': 'WORKS_APPS',
		'name': 'WORK_WHATSTHAT',
		'filter': 'apps',
		'link': 'https://play.google.com/store/apps/details?id=es.edu.android.whatsthat',
		'img_ref': 'img_whatsthat',
		'image': 'public/img/portfolio/6.jpg',
		'order': 6
	}];
	$scope.works = allWorks;
	
	$scope.filterWorks = function(filter) {
		$scope.workFilter = filter;
	};
	
	
	/*********** HIRING ************/
	$scope.jobs = [{
		key: 'ALERCE',
		place_link: 'https://www.google.fr/maps/@48.9356218,2.2721304,3a,75y,186.89h,87.47t/data=!3m6!1e1!3m4!1sntsWCwk1H-jtUUzX-nFYwQ!2e0!7i13312!8i6656',
		company_link: 'http://www.alerce-group.com',
		job_link: 'https://fr.linkedin.com/in/eduardoastolfi',
		tasks: [1, 2, 3]
	},
	{
		key: 'EXPECTRA',
		place_link: 'https://www.google.com/maps/@45.391266,1.389831,3a,75y,171.27h,78t/data=!3m6!1e1!3m4!1sn0BpbfS7srm4Y8P5bNxBng!2e0!7i13312!8i6656',
		company_link: 'http://www.expectra.fr',
		job_link: 'https://fr.linkedin.com/in/eduardoastolfi',
		tasks: [1, 2, 3]
	},
	{
		key: 'TRAGSA',
		place_link: 'https://www.google.com/maps/@40.43414,-3.630808,3a,75y,152.86h,81.09t/data=!3m6!1e1!3m4!1soZFp1Y6-W7jY2bfRUQPtYw!2e0!7i13312!8i6656',
		company_link: 'http://www.tragsa.es',
		job_link: 'https://fr.linkedin.com/in/eduardoastolfi',
		tasks: [1, 2, 3]
	},
	{
		key: 'EITJOBS',
		place_link: 'https://www.google.com/maps/@40.4413521,-3.6769918,3a,75y,12.61h,80.26t/data=!3m6!1e1!3m4!1sFWbQ7O_PtJSmc7COGONz5w!2e0!7i13312!8i6656',
		company_link: 'http://www.jobynet.com',
		job_link: 'https://fr.linkedin.com/in/eduardoastolfi',
		tasks: [1, 2, 3]
	}];
	
	/*********** CONTACT ************/
	$scope.sendingMail = false;
	$scope.emailError = false;
	$scope.emailSuccess = false;
	$scope.label_send_mail = 'CONTACT_FORM_SUBMIT_SEND';	
	$scope.doOnSubmit = function() {
		if ($scope.contactForm.$valid) {
			var form = $('form[name=contactForm]');
			
			$.ajax({
				type: form.attr('method'),
				url: form.attr('action'),
				data: form.serialize(),
				beforeSend: function() {
					$scope.label_send_mail = 'CONTACT_FORM_SUBMIT_SENDING';
					$scope.sendingMail = true;
				},
				success: function(data) {
					$scope.label_send_mail = 'CONTACT_FORM_SUBMIT_SEND';
					$scope.sendingMail = false;
					$scope.emailSuccess = true;
					$scope.contactForm.contactName = '';
					$scope.contactForm.contactMail = '';
					$scope.contactForm.contactContent = '';
					setTimeout(function() {
						$scope.emailSuccess = false;
						$scope.$apply();
					}, 3000);
					$scope.$apply();
				},
				error: function(error) {
					$scope.label_send_mail = 'CONTACT_FORM_SUBMIT_SEND';
					$scope.sendingMail = false;
					$scope.emailError = true;
					setTimeout(function() {
						$scope.emailError = false;
						$scope.$apply();
					}, 3000);
					$scope.$apply();
				}
			});
		} else {
			$scope.contactForm.contactName.$setDirty(true);
			$scope.contactForm.contactMail.$setDirty(true);
			$scope.contactForm.contactContent.$setDirty(true);
		}
	};
	
	/*********** GITHUB ************/
	GithubActivityService.events({
        user:'eastolfi',
        params:{
            access_token:'7b34f6b3f1f38b436d3f83bde439e455ad260d19',
            callback:'JSON_CALLBACK'
        }
    }).get().$promise.then(function(events){
        $scope.activity = events.data;
    });

    $scope.config = {
        limit: 5
    };

    /*********** TRANSLATE ************/
    var MyLanguage = function(_title, _icon, _key, _code) {
        this.title = _title;
        this.icon = _icon;
        this.key = _key;
        this.code = _code;

        this.isCurrent = function() {
            return this.key === $translate.currentLanguague;
        };
        this.setCurrent = function() {
            $translate.use(this.key);
            $translate.currentLanguague = this.key;
        };
    };
    $scope.languages = [
        new MyLanguage('LANG_EN_GB', 'gb', 'en_GB', 'ENG'),
        new MyLanguage('LANG_ES_ES', 'es', 'es_ES', 'SPA'),
        new MyLanguage('LANG_FR_FR', 'fr', 'fr_FR', 'FRE'),
        new MyLanguage('LANG_IT_IT', 'it', 'it_IT', 'ITA')
    ];
    $translate.currentLanguague = 'en_GB';
    
    /*
    $scope.toggleNav = $mdUtil.debounce(function() {
    	$mdSidenav("mainSideNav").toggle().then(function () {
            console.debug("toggle right is done");
      	});
  	}, 200);
  	
  	$scope.asideNavHandler = {
  		menu: $('#asideNav'),
  		trigger: null,
  		eventType: mobileCheck() ? 'touchstart' : 'click',
  		overlay: null,
  		
  		init: function() {
  			this.trigger = this.menu.find('a.aside-nav-trigger');
  			
  			this.overlay = $('<div>');
			this.overlay.addClass('aside-overlay');
			this.menu.append(this.overlay);
			
			this.trigger.on(this.eventType, this.toggle);
  		},
  		
  		toggle: function(ev) {
			ev.stopPropagation();
			ev.preventDefault();
			
			if(this.menu.hasClass('aside-nav-open')) {
				this.reset();
			}
			else {
				this.menu.removeClass('aside-nav-close');
				this.menu.addClass('aside-nav-open');
				this.overlay.on(this.eventType, this.close);
			}
  		},
  		
  		reset: function() {
  			this.menu.removeClass('aside-nav-open');
			this.menu.addClass('aside-nav-close');
  		},
  		
  		close: function() {
  			this.resetMenu();
			this.overlay.off(this.eventType, this);
  		}
  	};
  	
  	$scope.toggleAsideNav = function(ev) {
  		debugger;
  		
  		var menu = $('#asideNav');
  		var trigger = menu.find('a.aside-nav-trigger');
  		
			// event type (if mobile, use touch events)
		var eventtype = mobilecheck() ? 'touchstart' : 'click',
			resetMenu = function() {
				menu.removeClass('aside-nav-open');
				menu.addClass('aside-nav-close');
			},
			closeClickFn = function( ev ) {
				resetMenu();
				overlay.off(eventtype, closeClickFn);
			};

		var overlay = $('<div>');
		overlay.addClass('aside-overlay');
		menu.append(overlay);

		trigger.on(eventtype, function(ev) {
			ev.stopPropagation();
			ev.preventDefault();
			
			if(menu.hasClass('aside-nav-open')) {
				resetMenu();
			}
			else {
				menu.removeClass('aside-nav-close');
				menu.addClass('aside-nav-open');
				overlay.on(eventtype, closeClickFn);
			}
		});
  	};
  	*/
  	//$scope.toggleNav = 
}])
.controller('MainSideNavController', function ($scope, $timeout, $mdSidenav, $log) {
	$scope.close = function () {
  		$mdSidenav('mainSideNav').close().then(function () {
      		$log.debug("close RIGHT is done");
    	});
	};
});