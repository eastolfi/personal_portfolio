/* global angular */

angular.module('portfolio', ['ngResource', 'ui.bootstrap', 'ngMaterial', 'ngMessages',
	'pascalprecht.translate', 'github.activity', 'wu.masonry', 'popoverToggle', 
	'eastolfi.customPanel', 'eastolfi.asideMenu',
	'portfolio.index']);

angular.module('portfolio.index', []);
angular.module('ghService', []);


angular.module('portfolio').config(function($translateProvider, $mdThemingProvider) {
	$mdThemingProvider.theme('appMainTheme')
	.primaryPalette('red')
	.accentPalette('indigo')
	.warnPalette('deep-orange');
	//.backgroundPalette('deep-orange');
	/*
	$mdThemingProvider.theme('default')
    .primaryPalette('pink')
    .accentPalette('orange')
    .warnPalette('teal')
    .backgroundPalette('grey');*/
    /*
	$mdThemingProvider.theme('altTheme')
	.primaryPalette('brown');
	*/
	$mdThemingProvider.setDefaultTheme('appMainTheme');
	
	$translateProvider.useSanitizeValueStrategy('escape');
	
	$translateProvider.useStaticFilesLoader({
		prefix: 'langs/',
		suffix: '.json'
	});
	$translateProvider.preferredLanguage('en_GB');
});