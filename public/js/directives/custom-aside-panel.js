var AsideSlideHelper = {
    asideList: [],
    addAside: function(_item, options) {
        this.asideList.push(new AsideSlide(_item, options));
    },
    toggleOthers: function(_cb) {
        for (var i = 0; i < this.asideList.length; i++) {
            var _aside = this.asideList[i];

            if (_aside.isOpen()) {
                _aside.toggleClose();
            }
        }
        if (_cb && _cb instanceof Function) {
            _cb();
        }
    },
    isAsideMD: function() {
        if (window.matchMedia) {
            return window.matchMedia("(min-width: 768px)").matches;
        } else {
            return window.innerWidth > 768;
        }
    }
};

var AsideSlide = function(_item, positions) {
    var that = this;
    var _ele = _item;
    var _toggle = $(_ele).find('.side-toggle');
    var posXS = positions.XS;
    var posMD = positions.MD;
    
    this.getActualPosition = function() {
        if (AsideSlideHelper.isAsideMD()) {
            return posMD;
        } else {
            return posXS;
        }
    };
    
    this.toggleClose = function(e) {
        var _speed = e ? 500 : 200;
        if($(_ele).hasClass('opened')) {
            if (that.getActualPosition() == 'right') {
                $(_ele).animate({'left':'100%'}, _speed);
            } else if (that.getActualPosition() == 'bottom') {
                $(_ele).animate({'bottom':'-65%'}, _speed);
            }
            
            $(_ele).removeClass('opened');
        } else {
            AsideSlideHelper.toggleOthers(function() {
                if (that.getActualPosition() == 'right') {
                    $(_ele).animate({'left':'50%'});
                } else if (that.getActualPosition() == 'bottom') {
                    $(_ele).animate({'bottom':'0'});
                }
                
                $(_ele).addClass('opened');
            });
        }
    };

    this.isOpen = function() {
        return $(_ele).hasClass('opened');
    };

    _toggle.addClass('toggle-' + posXS + '-xs-' + (AsideSlideHelper.asideList.length + 1));
    _toggle.addClass('toggle-' + posMD + '-md-' + (AsideSlideHelper.asideList.length + 1));
    
    _toggle.on('click', this.toggleClose);
};

angular.module('eastolfi.customPanel', []).directive('eaCustomPanel', function() {
    return {
		restrict: 'A',
		link: function(scope, element, params) {
			var _params = params.eaCustomPanel;
            var _panels = [];
			
			if (_params) {
				_params = JSON.parse(_params);
			}
			
			var position = {};
			if (_params.position) {
			    
			} else {
			    position.XS = 'bottom';
			    position.MD = 'right';
			}
			
			// Añadimos los estilos para la posicion
			if (_params.position === 'top' || _params.position === 'right' || 
				_params.position === 'bottom' || _params.position === 'left') {
				element.addClass('append-side-' + _params.position);
			} else {
				element.addClass('append-side-xs-bottom');
				element.addClass('append-side-md-right');
			}

            var _items = element.find('.append-item');

            // Ocultamos el resto de elementos
            element.find('>*:not(.append-item)').css('display', 'none');

            
            

            for (var i = 0; i < _items.length; i++) {
                var _item = _items[i];

                AsideSlideHelper.addAside(_item, position);
            }


            //element.addClass('append-side');
			
			/*element.css('top', '205px');
			element.css('right', '-500px');*/

			/*for (var i = 0; i < element.find('.side-toggle').length; i++) {
				$(element.find('.side-toggle')[i]).css('top', (50*i - 1) + 'px');
			}
			*//*
			// TODO element -> fixed; onClick: move_parent & make !clicked display: none;
			element.find('.side-toggle').on('click', function() {
				var _parent = $(this).parent();
				if($(this).hasClass('opened')) {
					$(this).removeClass('opened');
					
					
					$(_parent).animate({'right':'-500px'});
				} else {
					var areOpen = $(element).find('.open');
                    for (var i = 0; i < areOpen.length; i++) {
                        $(areOpen[i]).removeClass('open');
                    }
                    $(this).addClass('opened');
					
					
					$(_parent).animate({'right':'-10px'});
				}
			});
			*/
		}
	};
});