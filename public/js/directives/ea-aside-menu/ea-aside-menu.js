'use strict';

var eaModule = angular.module('eastolfi.asideMenu', []);

eaModule.directive('eaAsideMenu', function() {
    return {
        strict: 'A',
        link: function(scope, element, params) {
            var openAsideMenu = function() {
                _target.removeClass('aside-nav-close');
            	_target.addClass('aside-nav-open');
            	
                $.each(_triggers, function(index, _trig) {
                    $(_trig).removeClass('aside-nav-trigger-close');
            	    $(_trig).addClass('aside-nav-trigger-open');
                });
            	
            	$('body').addClass('no-scroll');
            };
            
            var closeAsideMenu = function() {
                _target.removeClass('aside-nav-open');
            	_target.addClass('aside-nav-close');
            	
            	$.each(_triggers, function(index, _trig) {
                    $(_trig).removeClass('aside-nav-trigger-open');
                	$(_trig).addClass('aside-nav-trigger-close');
                });
            	
            	$('body').removeClass('no-scroll');
            };
            
            
            var _opts = scope.$eval(params.eaAsideMenu);
            var _target = $(element[0]);
            
            var _side = _opts.position;
            var _outsideClose = _opts.closeOnOutside;
            var _triggerId = _opts.trigger;
            var _hideInner = _opts.showInnerTrigger ? false : true;
            
            
            
  			var overlay = $('<div class="aside-overlay">');
  			
  			if (_outsideClose) {
  			    overlay.on('click', closeAsideMenu);//this.eventType
  			}
  			
			_target.append(overlay);
			
			_target.addClass('aside-nav');
			_target.addClass('aside-nav-' + _side);
			
			
			var _closeBtn = $('<a class="aside-menu-inner-trigger" ea-aside-trigger="'+_triggerId+'"><span>Close</span></a>');
			
			if (_hideInner) {
			    _closeBtn.addClass('hide-inner-trigger');
			}
			
			_target.append(_closeBtn);
			
			var _triggers = $('[ea-aside-trigger=' + _triggerId + ']'); // Move to directive
			for (var i = 0; i < _triggers.length; i++) {
			    var _trig = $(_triggers[i]);
			    
                _trig.on('click', function(ev) {
                    ev.stopPropagation();
        			ev.preventDefault();
        			
        			if(_target.hasClass('aside-nav-open')) {
        				closeAsideMenu(_trig);
        			}
        			else {
        				openAsideMenu(_trig);
        			}
                });
			}
        }
    };
});