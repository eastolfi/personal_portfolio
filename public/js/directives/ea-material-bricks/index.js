/* global angular */

"use strict";

var eaModule = angular.module('eastolfi.materialBricks', []);

var templateRegexp = /%\{([\w\d\s])+\}/ig;
var _compile = function(node, source) {
    //node.constructor.name.toLowerCase()
    let isLeaf = node.children().length ? false : true;
    
    if (isLeaf) {
        if (node.text()) {
            let compiled = node.text().replace(templateRegexp, match => {
                var property = match.replace('%\{', '').replace('\}', '');
                
                if (source[property] != null) {
                    return source[property];
                }
            });
            
            node.text(compiled);
        }
    } else {
	    let childs = node.children();
	    
	    for (let i = 0; i < childs.length; i++) {
	        _compile(angular.element(childs[i]), source);
	    }
    }
};

eaModule.directive('eaBrick', function() {
	return {
		scope: true,
		link: function($scope, element, attrs, masonryCtrl) {
			var source = attrs.eaBrickSource || null;
			var filter = attrs.eaBrickFilter || null;
			var template = element.clone();
			
			if (source !== null) {
			    var sourceArray = $scope[source];
			    
			    element.children().remove();
			    
			    // sourceArray array
			    for (let i = 0; i < sourceArray.length; i++) {
			        //console.log(sourceArray[i]);
			        if (template !== null) {
			            var item = template.clone();
			            
        			    _compile(item, sourceArray[i]);
        			    
        			    element.append(item);
        			} else {
        			    // Error
        			}
                }
			} else {
			    // Error
			}
			
// 			var targetId = opts.id;
			
// 			var that = this;
// 			//var ctrl = masonryCtrl;
// 			var scope = $scope;
// 			var container = $('#' + targetId);
			
// 			scope.filteredElements = [];
			
// 			$('#' + targetId).on('click', '[data-masonry-filter-item]', function(e) {
// 				container.find('li').removeClass('active');
// 				$(this).addClass('active');
				
// 				for (var i = 0; i < scope.filteredElements.length; i++) {
// 					if (scope.filteredElements[i] != null) {
// 						scope.works.splice(scope.filteredElements[i].order - 1, 0, scope.filteredElements[i]);
// 					}
// 				}
				
// 				scope.filteredElements = [];
// 				var category = $(this).data('masonry-filter-item');
// 				var lst = $(scope.works.clone());
// 				lst.each(function() {
// 					if (category !== '*' && this.filter.indexOf(category) === -1) {
// 						//scope.filteredElements[this.order - 1] = this;
// 						scope.filteredElements.push(this);
// 						scope.works.splice(this.order - scope.filteredElements.length, 1);
// 					}
// 				});
// 				scope.$apply();
// 			});
		}
	};
});