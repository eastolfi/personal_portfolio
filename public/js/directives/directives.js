/**
 * Created by Eduardo on 02/06/2015.
 */
angular.module('portfolio.index').directive('masonryFiltered', function() {
	function generateTemplate() {
		return '<div class="mf-filters"></div><div class="container-fluid mf-wrapper"></div>';
	}
	
	return {
		restrict: 'EA',
		template: generateTemplate(),
		link: function(scope, element, params) {
			var _filtersCont = element.find('.mf-filters');
			var _wrapperCont = element.find('.mf-wrapper');
			var _paramsMF = params.masonryFiltered;
			
			var _filters = [];
			if (_paramsMF) {
				_paramsMF = JSON.parse(_paramsMF);
				var _filterP = _paramsMF.filters;
				var _dataP = _paramsMF.data;
				
				if (_filterP.charAt(0) === '*') {
					_filterP = scope[_filterP.substr(1)];
				}
				
				if (_dataP.charAt(0) === '*') {
					_dataP = scope[_dataP.substr(1)];
				}
				
				if (typeof _filterP === 'object') {	//CAMBIAR
					_filters = _filterP;
				} else {
					_filters.push({
						filter: _filterP,
						label: _filterP
					});
				}
			}
						
			// Create Filters
			var _ul = $('<ul>');
			for (var i = 0; i < _filters.length; i++) {
				var _f = _filters[i];
				var _li = $('<li>');
				var _a = $('<a data-filter="" ng-click="doFilterMansonry()" click="fnc()">');
				
				//_a.attr('data-filter', _f.filter);
				//_a.attr('href', '');
				//_a.attr('ng-click', 'doFilterMansonry()');
				_a.html(_f.label);
				
				_a.appendTo(_li);
				_li.appendTo(_ul);
			}
			
			scope.$apply();
			
			
			scope.doFilterMansonry = function(e) {
				console.log('filter');
			};
			var fnc = scope.doFilterMansonry;
			
			_filtersCont.append(_ul);
			// Create Items
			var _ul = $('<ul>');
			for (var i = 0; i < _filters.length; i++) {
				var _f = _filters[i];
				var _li = $('<li>');
				var _a = $('<a>');
				
				_a.attr('data-filter', _f.filter);
				_a.attr('href', '');
				_a.html(_f.label);
				
				_a.appendTo(_li);
				_li.appendTo(_ul);
			}
			
			//_filtersCont.append(_ul);
		}
	}
});

angular.module('portfolio.index').directive('eaFilterMasonry', function() {
	return {
		require: 'masonry',
		scope: true,
		link: function($scope, element, attrs, masonryCtrl) {
			var opts = JSON.parse(attrs.eaFilterMasonry);
			
			var targetId = opts.id;
			
			var that = this;
			//var ctrl = masonryCtrl;
			var scope = $scope;
			var container = $('#' + targetId);
			
			scope.filteredElements = [];
			
			$('#' + targetId).on('click', '[data-masonry-filter-item]', function(e) {
				container.find('li').removeClass('active');
				$(this).addClass('active');
				
				for (var i = 0; i < scope.filteredElements.length; i++) {
					if (scope.filteredElements[i] != null) {
						scope.works.splice(scope.filteredElements[i].order - 1, 0, scope.filteredElements[i]);
					}
				}
				
				scope.filteredElements = [];
				var category = $(this).data('masonry-filter-item');
				var lst = $(scope.works.clone());
				lst.each(function() {
					if (category !== '*' && this.filter.indexOf(category) === -1) {
						//scope.filteredElements[this.order - 1] = this;
						scope.filteredElements.push(this);
						scope.works.splice(this.order - scope.filteredElements.length, 1);
					}
				});
				scope.$apply();
			});
		}
	};
});