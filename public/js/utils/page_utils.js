"use strict";

/*!
 * Start Bootstrap - Creative Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */
$(document).ready(function() {
	/*============================================
				Navigation Functions
	==============================================*/
	
	// Match nav sections while scrolling
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });
	
	// Animation on page scrolling - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });
	
	// Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a, .navbar-brand').click(function() {
        $('.navbar-toggle:visible').click();
    });
	
	// Fit the Main Header text
	$("h1").fitText(	//Removable
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );
	
	// Changes the nav on scroll
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    });
	
	// Initialize WOW.js Scrolling Animations
    new WOW().init();
	
	/*============================================
			End of Navigation Functions
	==============================================*/
	
	/*============================================
					Skills Functions
	==============================================*/
	var aboutColor = $('#about').css('backgroundColor');

	$('#skills').waypoint(function(){
		$('.chart').each(function(){
		$(this).easyPieChart({
				size:170,
				animate: 2000,
				lineCap:'butt',
				scaleColor: false,
				barColor: aboutColor,
				lineWidth: 10
			});
		});
	},{offset:'80%'});
	/*============================================
				End of Skills Functions
	==============================================*/
	
	/*============================================
				Projects Functions
	==============================================*/
	/*
	$('#projects-container').css({visibility:'visible'});

	
	var msrnContainer = $('#projects-container').masonry({
		itemSelector: '.project-item:not(.filtered)',
		columnWidth:350,
		isFitWidth: true,
		isResizable: true,
		isAnimated: !Modernizr.csstransitions,
		gutterWidth: 0
	});
	
	scrollSpyRefresh();
	waypointsRefresh();
	
	$('#filter-works a').click(function(e) {
		e.preventDefault();

		$('#filter-works li').removeClass('active');
		$(this).parent('li').addClass('active');

		var category = $(this).attr('data-filter');

		$('.project-item').each(function(){
			if($(this).is(category)){
				$(this).removeClass('filtered');
			}
			else{
				$(this).addClass('filtered');
			}

			//$('#projects-container').masonry('reload'); // DEPRECATED
			msrnContainer.masonry('reloadItems');
			msrnContainer.masonry();
		});

		scrollSpyRefresh();
		waypointsRefresh();
	});
	*/
	
	/* BORRAR
	var $grid = $('#projects-container').masonry({
		itemSelector: '.project-item:not(.filtered)',
		columnWidth: 350,
		isFitWidth: true,
		isResizable: true,
		isAnimated: !Modernizr.csstransitions
	});
	
	$('#filter-works a').on('click', function(e) {
		e.preventDefault();
		
		$('#filter-works li').removeClass('active');
		$(this).parent('li').addClass('active');
		
		var filter = $(this).attr('data-filter');
		
		//$($grid.find('.project-item')[0]).toggleClass('filtered');
		var items = $grid.find('.project-item');
		for (var i = 0; i < items.length; i++) {
            var item = $(items[i]);
            
            if (item.hasClass(filter) || filter === '*') {
                item.removeClass('filtered');
            } else {
                item.addClass('filtered');
            }
        }
		
		$grid.masonry('reloadItems');
		$grid.masonry();
	});
	
	scrollSpyRefresh();
	waypointsRefresh();
	*/
	/*============================================
			End of Projects Functions
	==============================================*/
	
	/*============================================
				Backstretch Functions
	==============================================*/	
	/*
	$.backstretch('public/img/header-bg.jpg');

	$('body').append('<img class="preload-image" src="public/img/contact-bg.jpg" style="display:none;"/>');

	$('#about').waypoint(function(direction){
	
		if($('.preload-image').length){$('.preload-image').remove();}
		
		$('.backstretch').remove();
	
		if (direction=='down'){
			$.backstretch('public/img/contact-bg.jpg');
		}else{
			$.backstretch('public/img/header-bg.jpg');
		}
	});
	*/
	/*============================================
			End of Backstretch Functions
	==============================================*/
	
	/*============================================
				Refreshing Functions
	==============================================*/
	/*
	function scrollSpyRefresh(){
		setTimeout(function(){
			$('body').scrollspy('refresh');
		},1000);
	}
	
	function waypointsRefresh(){
		setTimeout(function(){
			//$.waypoints('refresh'); // DEPRECATED
			Waypoint.refreshAll();
		},1000);
	}
	*/
	/*============================================
			End of Refreshing Functions
	==============================================*/
	
	/*============================================
				GitHub Functions
	==============================================*/
	
	// Initialize Github Activity - Require Mustache
	/*
	GitHubActivity.feed({
		username: "eastolfi",
		selector: "#my-github",
		limit: 5
	});
	*/
	// Show / Hide the Github Activity panel
	var toggleRightMedia = window.matchMedia('(min-width: 768px)');
	$('#toggle-github').click(function(){
		if($(this).hasClass('opened')) {
			$(this).removeClass('opened');
			if (toggleRightMedia.matches) {				
				$('#github-profile').animate({'right':'-500px'});
				$('#my-github').css('height', '');
			} else {
				
				$('#github-profile').animate({'top':'101%'});
				$('#my-github').css('height', '');
			}		
		}else {
			$(this).addClass('opened');
			if (toggleRightMedia.matches) {				
				$('#github-profile').animate({'right':'-10px'});
				if ($(window).height() - 150 < 480) {
					$('#my-github').height(($(window).height() - 150) * 0.7);
				}
			} else {				
				$('#github-profile').animate({'top':'25%'});
				if ($(window).height() - 150 < 480) {
					$('#my-github').height(($(window).height() - 150) * 0.7);
				}
			}
		}
	});
	
	/*============================================
				End of GitHub Functions
	==============================================*/
});



Array.prototype.searchObject = function(field, value) {
	var res = {
		index: -1,
		element: null
	};
	
	$.grep(this, function(element, idx) {
		if (element[field] == value) {
			res.index = idx;
			res.element = element;
			
			return true;
		} else {
			return false;
		}
	});
	
	return res;
};

Array.prototype.clone = function() {
	return this.slice(0);
};

//http://stackoverflow.com/a/11381730/989439
var mobileCheck = function() {
	var check = false;
	(function(a){if(/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};