var express = require('express'),
	http = require('http'),
	path = require('path'),
	Mailgun = require('mailgun-js');
	
var app = express();

app.use(express.compress({
    threshold : 0,
    filter: function(req, res){
        return /json|text|javascript|dart|image\/svg\+xml|application\/x-font-ttf|application\/vnd\.ms-opentype|application\/vnd\.ms-fontobject/.test(res.getHeader('Content-Type'));
    }
}));
//app.use(express.favicon(__dirname + '/public/img/icons/favicon.ico'));
//app.use(express.compress({threshold: 0}));
app.use(express.urlencoded());
app.use(express.json());
app.use(express.methodOverride());

app.set('views', __dirname + '/public/views');
app.set('view engine', 'pug');

app.use('/public', express.static(path.join(__dirname, '/public')));
app.use('/bower', express.static(path.join(__dirname, '/bower_components')));
app.use('/langs', express.static(path.join(__dirname, '/public/langs')));
app.use('/template', express.static(path.join(__dirname, '/public/templates')));


app.get('/', function(req, res) {
	res.render('index');
});

var api_key = 'key-c68b20f4e7b02d40ad62fdd7ecf67b6c';
var domain = 'sandboxc3ed3b1e84c24718911bc4eb084640f9.mailgun.org';
app.post('/contact', function(req, res) {
	var from = req.body.contactMail;
	var subject = "✉ Web Contact - From: " + req.body.contactName;//&#7360;
	var content = req.body.contactContent;
	
	var mailgun = new Mailgun({apiKey: api_key, domain: domain});

    var data = {
      from: from,
      to: "eduardo.astolfi91@gmail.com",
      subject: subject,
      html: content
    };

    //Invokes the method to send emails given the above data with the helper library
    mailgun.messages().send(data, function (err, body) {
        //If there is an error, render the error page
        if (err) {
            console.log("got an error: ", err);
            throw err;
        }
        //Else we can greet    and leave
        else {
            console.log(body);
            res.send('donde');
        }
    });
});

var PORT = process.env.PORT || 3000;
http.createServer(app).listen(PORT, function() {
	console.log('Servidor arrancado en el puerto ' + PORT);
});